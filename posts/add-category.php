<?php

require "../base/db.php";
require "../base/functions.php";


$name = isset($_POST['name']) ? $_POST['name'] : '';

$parent_id = isset($_POST['parent_id']) ? $_POST['parent_id'] : null;

if(empty($parent_id)) {
    $data = ['name' => $name];
} else {
    $data = ['name' => $name,  'parent_id' => $parent_id];
}






$category = insert('categories', $data, $conn);

header("Location: /categories.php");