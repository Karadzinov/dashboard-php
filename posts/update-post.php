<?php

require "../base/db.php";
require "../base/functions.php";


$title = isset($_POST['title']) ? $_POST['title'] : '';
$image = isset($_POST['image']) ? $_POST['image'] : '';
$description = isset($_POST['description']) ? $_POST['description'] : '';
$id = isset($_POST['id']) ? $_POST['id'] : '';

$data = ['image' => $image, 'description' => $description, 'title' => $title];

$post = updateRow('posts', $id, $data, $conn);

if($post) {
    header("Location: /edit-post.php?post=$id");
} else {
    header("Location: /post.php");
}