<?php

require "../base/db.php";
require "../base/functions.php";


$title = isset($_POST['title']) ? $_POST['title'] : '';
$userId = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : '';
$image = isset($_POST['image']) ? $_POST['image'] : '';
$description = isset($_POST['description']) ? $_POST['description'] : '';

$data = ['image' => $image, 'description' => $description, 'user_id' => $userId, 'title' => $title];


$post = insert('posts', $data, $conn);

header("Location: /post.php");