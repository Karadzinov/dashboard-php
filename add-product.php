<?php
require "base/functions.php";
require "main-header.php";

require "base/db.php";


$result = selectAll('categories', $conn);

$categories = [];

while ($cat = mysqli_fetch_assoc($result)) {
    $categories[] = $cat;
}


$users = [];
$result = selectAll('users',  $conn);
while($user = mysqli_fetch_assoc($result)) {
    $users[] = $user;
}
?>

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card z-index-0 fadeIn3 fadeInBottom">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                            <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">Add Product</h4>
                            <div class="row mt-3">

                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form role="form" class="text-start" method="post" action="/products/add.php" enctype="multipart/form-data">


                            <div class="input-group input-group-outline my-3 ">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" name="name">
                            </div>


                            <div class="input-group input-group-outline my-3 ">
                                <label class="form-label">Image</label>
                                <input type="file" class="form-control" name="image">
                            </div>


                            <div class="input-group input-group-outline my-3 ">
                                <label class="form-label">Quantity</label>
                                <input type="text" class="form-control" name="quantity">
                            </div>

                            <div class="input-group input-group-outline my-3 ">
                                <label class="form-label">Price</label>
                                <input type="text" class="form-control" name="price">
                            </div>

                            <div class="input-group input-group-outline mb-3 ">

                                <textarea name="description" id="editor-product"></textarea>
                            </div>

                            <div class="input-group input-group-outline mb-3 ">
                                <select name="user_id" class="form-control">
                                    <option value="">Select User</option>
                                    <?php
                                        foreach($users as $user) {
                                            echo ' <option value="'.$user['id'].'">'.$user['name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>


                            <div class="input-group input-group-outline my-3 is-filled">
                                <label class="form-label">Select Category</label>
                                <select class="form-control" name="cat_id" id="parent_option">
                                    <?php
                                    echo makeOptions($categories, $conn);
                                    ?>
                                </select>

                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-primary w-100 my-4 mb-2">Add</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php require "main-footer.php"; ?>