<?php


require "../base/db.php";
require "../base/functions.php";

$file = $_FILES;


$id = isset($_POST['id']) ? $_POST['id'] : '';
$name = isset($_POST['name']) ? $_POST['name'] : '';
$image = imageUpload($file);
$cat_id = isset($_POST['cat_id']) ? $_POST['cat_id'] : '';
$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
$quantity = isset($_POST['quantity']) ? $_POST['quantity'] : '';
$price = isset($_POST['price']) ? $_POST['price'] : '';
$description = isset($_POST['description']) ? $_POST['description'] : '';


$data = [
    'name'          => $name,
    'image'         => $image,
    'cat_id'        => $cat_id,
    'user_id'       => $user_id,
    'quantity'      => $quantity,
    'price'         => $price,
    'description'   => $description
];


$product = updateRow('products', $id, $data, $conn);

if($product) {
    header("Location: /products.php");
} else {
    header("Location: /edit-products.php?id=$id");

}