<?php

require "base/functions.php";
require "base/db.php";

if(!$_SESSION['user']) {
    header('Location: /login.php');
}

$products = selectAll('products', $conn);



require "main-header.php";
?>

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Products</h6>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Image</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Name</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Price</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Qunatity</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Category</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">User</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Description</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Created at</th>
                                    <th class="text-secondary opacity-7"></th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php


                                while($product = mysqli_fetch_assoc($products)) {

                                    $user = getSelect('users', $product['user_id'], $conn);
                                    $category = getSelect('categories', $product['cat_id'], $conn);



                                    $dateCreated =  date('d-M-Y', strtotime($product['created_at']));


                                    echo '
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
                                         
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm"><a href="/edit-product.php?id='.$product['id'].'">'.$product['id'].'</a></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                       <img src="/uploads/'.$product['image'].'" class="avatar avatar-sm me-3 border-radius-lg" alt="'.$product['name'].'">
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">'.$product['name'].'</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">$'.$product['price'].'</span>
                                    </td>
                                    
                                     <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">'.$product['quantity'].'</span>
                                    </td>
                                    
                                      <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">'.$category['name'].'</span>
                                    </td>
                                    
                                    
                                     <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">'.$user['name'].'</span>
                                    </td>
                                    
                                    
                                    
                                       <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">'.strip_tags(limit_text($product['description'], 50)).'</span>
                                    </td>
                                    
                                   
                                   
                                     <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">'.$dateCreated.'</span>
                                    </td>
                                    
                                    
                                </tr>
                                ';
                                }

                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12">
                <a href="/add-product.php" class="btn btn-outline-primary">Add product</a>
            </div>
        </div>
    </div>



<?php require "main-footer.php"; ?>