<?php

require "../base/db.php";
require "../base/functions.php";


$name = isset($_POST['name']) ? $_POST['name'] : '';
$image = isset($_POST['image']) ? $_POST['image'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$employed = isset($_POST['employed']) ? $_POST['employed'] : '';
$title = isset($_POST['title']) ? $_POST['title'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';


$data = ['name' => $name, 'image' => $image, 'email' => $email, 'employed' => $employed, 'title' => $title, 'password' => $password];

$user = insert('users', $data, $conn);

header("Location: /users.php");