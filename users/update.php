<?php
require "../base/db.php";
require "../base/functions.php";


$name = isset($_POST['name']) ? $_POST['name'] : '';
$image = isset($_POST['image']) ? $_POST['image'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$employed = isset($_POST['employed']) ? $_POST['employed'] : '';
$title = isset($_POST['title']) ? $_POST['title'] : '';
$id = isset($_POST['id']) ? $_POST['id'] : '';




$data = ['name' => $name, 'image' => $image, 'email' => $email, 'employed' => $employed, 'title' => $title];


$user = updateRow('users', $id, $data, $conn);

if($user) {
    header("Location: /user.php?user=$id");
} else {
    header("Location: /users.php");
}


