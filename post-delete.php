<?php

require "base/db.php";
require "base/functions.php";

$id = isset($_GET['id']) ? $_GET['id'] : '';

$post = deleteRow('posts', $id, $conn);


header("Location: /post.php");