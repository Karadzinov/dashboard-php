<?php

define('DB_NAME', 'dashboard');
define('DB_USERNAME', 'homestead');
define('DB_PASS', 'secret');
define('DB_HOST', 'localhost');

$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


