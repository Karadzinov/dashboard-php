<?php

require 'db.php';
require "functions.php";

$name       = isset($_POST['name']) ? $_POST['name'] : '';
$email      = isset($_POST['email']) ? $_POST['email'] : '';
$password   = isset($_POST['password']) ? $_POST['password'] : '';
$agree      = isset($_POST['agree']) ? 1 : 0;

$data = ['name' => $name, 'email' => $email, 'password' => $password, 'agree' => $agree];

$checkInsert = insert('users', $data, $conn);

if($checkInsert) {
    header('Location: /login.php');
}


