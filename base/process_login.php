<?php
session_start();
require "db.php";
require "functions.php";


$email = isset($_POST['email']) ? $_POST['email'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';

$data = ['email' => $email, 'password' => $password];

$user = checkLogin($data, $conn);

if($user) {
    $_SESSION['user'] = $user;
    header('Location: /index.php');
} else {
    header('Location: /login.php?error=401');
}



