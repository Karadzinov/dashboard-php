<?php
session_start();


function insert($table, $data, $conn)
{
    $keys = [];
    $values = [];
    foreach ($data as $index => $value) {
        $keys[] = $index;
        $values[] = $index === 'password' ? "sha('$value')" : "'$value'";
        var_dump($value);
    }
    $keys = implode(',', $keys);
    $values = implode(',', $values);

    $sql = "INSERT INTO $table ($keys) VALUES ($values)";


    if (mysqli_query($conn, $sql)) {

        return true;
    }


    return mysqli_error($conn);
}


function checkLogin($data, $conn)
{

    $conditions = [];
    $i = 0;
    foreach ($data as $index => $value) {

        if ($i == 0) {
            $conditions[] = $index === 'password' ? "WHERE $index = sha('$value')" : "WHERE $index = '$value'";
        } else {
            $conditions[] = $index === 'password' ? "AND $index = sha('$value')" : "AND $index = '$value'";
        }

        $i++;

    }
    $condition = implode(' ', $conditions);

    $sql = "SELECT * FROM users $condition";

    $result = mysqli_query($conn, $sql);

    return mysqli_fetch_assoc($result);

}

function selectAll($table, $conn)
{
    $sql = "SELECT * FROM $table";
    $result = mysqli_query($conn, $sql);
    return $result;
}

function getSelect($table, $user_id, $conn)
{
    $sql = "SELECT * FROM $table WHERE id=$user_id";
    $result = mysqli_query($conn, $sql);
    return mysqli_fetch_assoc($result);
}

function selectPosts($table, $post_id, $conn)
{
    $sql = "SELECT * FROM $table WHERE id=$post_id";
    $result = mysqli_query($conn, $sql);
    return mysqli_fetch_assoc($result);
}

function updateRow($table, $id, $data, $conn)
{

    $condition = '';
    foreach ($data as $index => $value) {

        $condition .= " $index = '$value',";
    }

    $condition = substr($condition, 0, -1);

    $sql = "UPDATE $table SET $condition WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    return $result;
}

function deleteRow($table, $id, $conn)
{
    $sql = "DELETE FROM $table WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    return $result;
}


function createList($categories, $conn)
{

    $lists = '<ul>';
    foreach ($categories as $category) {
        if ($category['parent_id'] === null) {
            $lists .= createSubCategory($category, $conn);
        }
    }
    $lists .= "</ul>";
    return $lists;

}

function createSubCategory($category, $conn)
{
    $id = $category['id'];

    $list = '<li>
                <a href="/edit-category.php?cat_id=' . $category['id'] . '">' . $category['name'] . '</a> 
                <a href="/delete-category.php?cat_id=' . $category['id'] . '"> <span class="text-danger">x</span> </a>
            </li>';
    $children = selectChildren($id, $conn);


    $count = count($children);

    if ($count > 0) {
        $list .= '<ul>';
        foreach ($children as $child) {
            $list .= createSubCategory($child, $conn);
        }
        $list .= "</ul>";
    }
    return $list;


}


function selectChildren($id, $conn)
{
    $results = mysqli_query($conn, "SELECT * FROM categories WHERE parent_id='$id'");
    $children = [];
    while ($child = mysqli_fetch_assoc($results)) {
        $children[] = $child;
    }
    return $children;
}


function makeOptions($categories, $conn)
{

    $lists = '';
    foreach ($categories as $category) {
        if ($category['parent_id'] === null) {
            $lists .= createSubCategoryOption($category, $conn);
        }
    }


    return $lists;
}

function createSubCategoryOption($category, $conn, $dash = 0, $print_dash = '')
{


    for ($i = 0; $i < $dash; $i++) {
        $print_dash .= '-';
    }


    $id = $category['id'];
    $list = '<option value="' . $category['id'] . '">' . $print_dash . ' ' . $category['name'] . '</option>';
    $children = selectChildren($id, $conn);

    $count = count($children);


    if ($count > 0) {

        foreach ($children as $child) {
            $list .= createSubCategoryOption($child, $conn, $dash + 1);


        }
    }
    return $list;
}


function imageUpload($file)
{

    $target_dir = "../uploads/";

    $randNumber = rand(100, 20000);
    $target_file = $target_dir . $randNumber . basename($file["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


    $check = getimagesize($file["image"]["tmp_name"]);


    if ($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";

    } else {
        return "File is not an image.";
    }


// Check if file already exists
    if (file_exists($target_file)) {
        return "Sorry, file already exists.";
    }

// Check file size
    if ($file["image"]["size"] > 5000000) {
        return "Sorry, your file is too large.";

    }

// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
        return "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

    }


    if (move_uploaded_file($file["image"]["tmp_name"], $target_file)) {
        return $randNumber . $file["image"]["name"];
    } else {
        return "Sorry, there was an error uploading your file.";
    }


}


function limit_text($text, $len) {
    if (strlen($text) < $len) {
        return $text;
    }
    $text_words = explode(' ', $text);
    $out = null;


    foreach ($text_words as $word) {
        if ((strlen($word) > $len) && $out == null) {

            return substr($word, 0, $len) . "...";
        }
        if ((strlen($out) + strlen($word)) > $len) {
            return $out . "...";
        }
        $out.=" " . $word;
    }
    return $out;
}