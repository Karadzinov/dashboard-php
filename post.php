<?php

require "base/functions.php";
require "base/db.php";

if (!$_SESSION['user']) {
    header('Location: /login.php');
}

$posts = selectAll('posts', $conn);


require "main-header.php";
?>

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Posts</h6>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Image
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Title
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Description
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        User ID
                                    </th>
                                    <th class="text-secondary opacity-7"></th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php


                                while ($post = mysqli_fetch_assoc($posts)) {


                                    echo '
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
                                            <div>
                                                <img src="' . $post['image'] . '" class="avatar avatar-sm me-3 border-radius-lg" alt="user1">
                                            </div>
                          
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">' . $post['title'] . '</p>
                                    <td class="align-middle text-center">
                                        <p class="text-secondary text-xs font-weight-bold">' . $post['description'] . '</p>
                                    </td>
                                    <td class="align-middle text-center">
                                        <p class="text-secondary text-xs font-weight-bold">' . $post['user_id'] . '</p>
                                    </td>
                                     <td class="align-middle">
                                        <a href="edit-post.php?post='.$post['id'].'" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                                            Edit
                                        </a>
                                    </td>
                                     <td class="align-middle">
                                        <a href="/post-delete.php?id='.$post['id'].'" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Delete user">
                                            Delete
                                        </a>
                                    </td>
                                
                                </tr>
                                ';
                                }

                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12">
                <a href="/create-post.php" class="btn btn-outline-primary">Create new post</a>
            </div>
        </div>
    </div>


<?php require "main-footer.php"; ?>