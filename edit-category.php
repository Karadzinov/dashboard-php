<?php

require "base/functions.php";
require "base/db.php";

if (!$_SESSION['user']) {
    header('Location: /login.php');
}


$cat_id = isset($_GET['cat_id']) ? $_GET['cat_id'] : '';


$category = selectPosts('categories', $cat_id, $conn);

$result = selectAll('categories', $conn);

$categories = [];

while ($cat = mysqli_fetch_assoc($result)) {
    $categories[] = $cat;
}

require "main-header.php";
?>


<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card z-index-0 fadeIn3 fadeInBottom">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                        <h4 class="text-white font-weight-bolder text-center mt-2 mb-0"><?php echo $category['name']; ?></h4>
                        <div class="row mt-3">

                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form role="form" class="text-start" method="post" action="/posts/update-post.php">
                        <input type="hidden" name="id" value="<?php echo $category['id']; ?>">


                        <div class="input-group input-group-outline my-3 is-filled">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control" name="name"
                                   value="<?php echo $category['name']; ?>">
                        </div>


                        <div class="input-group input-group-outline my-3 is-filled">
                            <label class="form-label">Parent Category</label>
                            <select class="form-control" name="parent_id" id="parent_option">
                                <?php
                                        echo makeOptions($categories, $conn);
                                ?>
                            </select>

                        </div>


                        <div class="text-center">
                            <button type="submit" class="btn bg-gradient-primary w-100 my-4 mb-2">Update</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require "main-footer.php"; ?>
