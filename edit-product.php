<?php

require "base/functions.php";
require "base/db.php";

if (!$_SESSION['user']) {
    header('Location: /login.php');
}

$product_id = isset($_GET['id']) ? $_GET['id'] : '';
$product = selectPosts('products', $product_id, $conn);

$result = selectAll('categories', $conn);

$categories = [];

while ($cat = mysqli_fetch_assoc($result)) {
    $categories[] = $cat;
}


$users = [];
$result = selectAll('users',  $conn);
while($user = mysqli_fetch_assoc($result)) {
    $users[] = $user;
}

require "main-header.php";
?>


    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card z-index-0 fadeIn3 fadeInBottom">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                            <h4 class="text-white font-weight-bolder text-center mt-2 mb-0"><?php echo $product['name']; ?></h4>
                            <div class="row mt-3">

                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form role="form" class="text-start" method="post" action="/products/update.php" enctype="multipart/form-data">


                            <input type="hidden" name="id" value="<?php echo $product['id']; ?>">
                            <div class="input-group input-group-outline my-3 focused is-focused">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" value="<?php echo $product['name']; ?>">
                            </div>


                            <div class="input-group input-group-outline my-3 ">
                                <p> <img src="/uploads/<?php echo $product['image']; ?>" class="avatar avatar-lg me-3 border-radius-lg" alt="<?php echo $product['name']; ?>"></p>
                            </div>

                            <div class="input-group input-group-outline my-3 focused is-focused">
                                <label class="form-label">Image</label>
                                <input type="file" class="form-control" name="image">
                            </div>

                            <div class="input-group input-group-outline my-3 focused is-focused">
                                <label class="form-label">Quantity</label>
                                <input type="text" class="form-control" name="quantity" value="<?php echo $product['quantity']; ?>">
                            </div>

                            <div class="input-group input-group-outline my-3 focused is-focused">
                                <label class="form-label">Price</label>
                                <input type="text" class="form-control" name="price" value="<?php echo $product['price']; ?>">
                            </div>

                            <div class="input-group input-group-outline mb-3 focused is-focused">

                                <textarea name="description" id="editor-product"><?php echo $product['description']; ?></textarea>
                            </div>

                            <div class="input-group input-group-outline mb-3 focused is-focused">
                                <select name="user_id" class="form-control">
                                    <option value="">Select User</option>
                                    <?php
                                    foreach($users as $user) {
                                        $selected = $user['id'] === $product['user_id'] ? 'selected' : '';
                                        echo ' <option value="'.$user['id'].'" '.$selected.'>'.$user['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>


                            <div class="input-group input-group-outline my-3 is-filled">
                                <label class="form-label">Select Category</label>
                                <select class="form-control" name="cat_id" id="cat_option">
                                    <?php
                                    echo makeOptions($categories, $conn);
                                    ?>
                                </select>

                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-primary w-100 my-4 mb-2">Add</button>
                            </div>

                        </form>


                        <form action="/products/delete.php?id=<?php echo $product['id']; ?>" method="post">
                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-danger w-100 my-4 mb-2">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $("#cat_option").val("<?php echo $product['cat_id'];?>").attr("selected","selected");
    </script>
<?php require "main-footer.php"; ?>